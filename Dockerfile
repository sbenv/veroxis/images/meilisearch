FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.1

ENV MEILI_NO_ANALYTICS=true

COPY "meilisearch-bin" "/usr/local/bin/meilisearch"
COPY "meilitool-bin" "/usr/local/bin/meilitool"

RUN apk add --no-cache "dumb-init"

ENTRYPOINT ["dumb-init", "--"]

CMD ["meilisearch"]
